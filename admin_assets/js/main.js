$(".insert_data").click(function(){
    if($('#youtube_url').val() == ""){
        $('#youtube_url').css('border-color', 'red');
        return false;
    }
});
$("#youtube_url").keyup(function(){
    $("input").css("border-color", "transparent");
});
$('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});
$('.alert').delay(15000).fadeOut(400)

$(document).ready(function(){
		
		$('#add-image-div').hide();
		
		$("#add-image").on('click',function(){
			$('#add-image-div').toggle();
		});
	});
			$("#add_data").click(function(){ 
    $("#add_data_div :last").clone().appendTo("#add_data_div");
	
	
});
$(document).ready(function () {

    $("#add_schedule_class").validate({
        rules: {
            class_day: {
                required: {
                    depends: function(element) {
                        return $("#class_day").val() == '';
                    }
                }
            },
            class_name: {
                required: true
            },
            class_from: {
                required: true
            }
        },
        messages: {
        class_day: 'Please select <b>Class day</b>.',
        class_name: 'Please enter  <b>Class name</b>.',
        class_from: 'Please enter  <b>Class from time</b>.',
        }
    });
    
    $("#update_schedule_class").validate({
        rules: {
            class_name: {
                required: true
            },
            class_from: {
                required: true
            }
        },
        messages: {
        class_name: 'Please enter  <b>Class name</b>.',
        class_from: 'Please enter  <b>Class from time</b>.',
        }
    });

});	