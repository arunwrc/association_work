<?php
class Media_model extends CI_Model{

    function insert_video($data){
        $this->db->insert('video_gallery', $data);
    }
    function list_video($limit,$start){
        $this->db->order_by("id","desc");
		if(@$limit)
		$this->db->limit($limit, $start);
        $query = $this->db->get('video_gallery');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
       
        return $query->result();
    }
    function get_video_by_id($id){
        $query = $this->db->get_where('video_gallery',array('id' => $id));
        return $query->row_array();
    }
    public function update_video($data,$id){
        $this->db->where('id',$id);
        $this->db->update('video_gallery',$data);
    }
    public function delete_video($id){
        $this->db->delete('video_gallery', array('id' => $id));
    }
	function insert_image($data){
        $this->db->insert('image_gallery', $data);
    }
	function list_image($limit,$start,$id){
		$this->db->where('image_category_id',$id);
		$this->db->limit($limit, $start);
        $query = $this->db->get("image_gallery");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
		/*$this->db->where('image_category_id',$id);
        $this->db->order_by("id","desc");
        $query = $this->db->get('image_gallery');
        return $query->result();*/
    }
	function list_image_category($limit,$start){
		 $this->db->join('image_gallery_category','image_gallery_category.id = image_gallery.image_category_id ','left');
        $this->db->group_by('image_category_id');
		$this->db->limit($limit, $start);
        $query = $this->db->get("image_gallery");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	public function record_count_category() {
		 $this->db->group_by('image_category_id');
        return $this->db->count_all_results("image_gallery");
    }
	public function record_count_video() {
		
        return $this->db->count_all_results("video_gallery");
    }
	
	function list_image_(){
		
       $query = $this->db->get('image_gallery');
       return $query->result();
    }
	function list_image_id($id){
		$this->db->where('image_category_id',$id);
        $this->db->order_by("id","desc");
        $query = $this->db->get('image_gallery');
        return $query->result();
    }
	 public function record_count($id) {
		 $this->db->where('image_category_id',$id);
        return $this->db->count_all_results("image_gallery");
    }
	public function delete_image($id){
        $this->db->delete('image_gallery', array('id' => $id));
    }
	 function get_image_by_id($id){
        $query = $this->db->get_where('image_gallery',array('id' => $id));
        return $query->row_array();
    }
	 public function update_image($data,$id){
        $this->db->where('id',$id);
        $this->db->update('image_gallery',$data);
    }
	public function insert_category($data){
        $this->db->insert('image_gallery_category', $data);
    }
	public function get_list_array_with_null($table,$id,$name,$filter=false,$order_by=false)
        {
                $this->db->select($id.','.$name);
                $this->db->from($table);
                if($filter)
                    $this->db->where($filter);
				if($order_by)
				$this->db->order_by($order_by,'ASC');
                $query = $this->db->get();
                $list = array(''=>'--Select One--');
                if($query->num_rows() > 0)
                {
                        $rows = $query->result();
                        for($i=0;$i<count($rows);$i++){
                              $list[$rows[$i]->$id]=$rows[$i]->$name;  
                        }
                }
                return $list;
        }
	function list_category(){
        $this->db->order_by("id","desc");
        $query = $this->db->get('image_gallery_category');
        return $query->result();
    }
	public function delete_category($id){
        $this->db->delete('image_gallery_category', array('id' => $id));
    }
	 function get_category_by_id($id){
        $query = $this->db->get_where('image_gallery_category',array('id' => $id));
        return $query->row_array();
    }
	 public function update_category($data,$id){
        $this->db->where('id',$id);
        $this->db->update('image_gallery_category',$data);
    }
}