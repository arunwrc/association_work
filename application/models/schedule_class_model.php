<?php
/**
 * Created by PhpStorm.
 * User: arun
 * Date: 01/08/15
 * Time: 9:35 PM
 */
class Schedule_class_model extends CI_Model{

    public function list_all(){
         $this->db->order_by("id","asc");
         $query = $this->db->get('schedule_class');
         return $query->result();
    }
    function insert_data($data){
        $this->db->insert('schedule_class_time', $data);
    }
    function list_all_classes(){
        $this->db->order_by("id","asc");
        $query = $this->db->get('schedule_class_time');
        return $query->result();
    }
    function list_classes_by_id($id){
        $query = $this->db->get_where('schedule_class_time',array('schedule_class_id' => $id));
        return $query->result();
    }
    function list_class_times_by_id($id){
        $query = $this->db->get_where('schedule_class_time',array('id' => $id));
        return $query->result();
    }
    public function update_data($id,$data){
        $this->db->where('id',$id);
        $this->db->update('schedule_class_time',$data);
    }
    public function delete_data($id){
        $this->db->delete('schedule_class_time', array('id' => $id));
    }

}