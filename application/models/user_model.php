<?php
/**
 * Created by PhpStorm.
 * User: arun
 * Date: 01/08/15
 * Time: 9:35 PM
 */
class User_model extends CI_Model{

    public function login($username,$password){
        $condition=array(
            'username'=>$username,
            'password'=>$password
        );
        $this->db->select()->from('users')->where($condition);
        $query=$this->db->get();
        return $query->first_row('array');
    }

//    public function update($data){
//        $this->db->where('id',$this->input->post('id'));
//        $this->db->update('users',$data);
//    }
//
//    public function get_by_id($id){
//        $query = $this->db->get_where('users',array('id' => $id));
//        return $query->row_array();
//    }
//    public function recordlogin($userid){
//        $data['last_login'] =  date('Y-m-d H:i:s');
//        $this->db->where('id',$userid);
//        $this->db->update('users', $data);
//    }
//    public function logout($userid){
//        $data['last_logoff'] =  date('Y-m-d H:i:s');
//        $this->db->where('id',$userid);
//        $this->db->update('users', $data);
//
//    }
}