<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['login'] = 'Login/login_form';
$route['login_action'] = 'Login/login_action';
$route['logout'] = 'Login/logout';
$route['default_controller'] = 'Menu/home';

$route['admin'] = 'Admin/admin_dashboard';
$route['admin/add-video'] = 'Admin/add_video';
$route['add_video_action'] = 'Admin/add_video_action';
$route['admin/list-video'] = 'Admin/list_video';
$route['admin/list-video/(:any)'] = 'Admin/list_video/$1';
$route['admin/update-video/(:any)'] = 'Admin/update_video/$1';
$route['update_video_action'] = 'Admin/update_video_action';
$route['admin/delete-video/(:any)'] = 'Admin/delete_video_action/$1';

$route['admin/add-image'] = 'Admin/add_image';
$route['add_image_action'] = 'Admin/add_image_action';
$route['admin/list-image/(:any)'] = 'Admin/list_image/$1';
$route['admin/list-image/(:any)/(:any)'] = 'Admin/list_image/$1/$2';
$route['admin/delete-image/(:any)'] = 'Admin/delete_image_action/$1';
$route['admin/update-image/(:any)'] = 'Admin/update_image/$1';
$route['update_image_action'] = 'Admin/update_image_action';

$route['admin/list-image-category'] = 'Admin/list_image_category';
$route['admin/list-image-category/(:any)'] = 'Admin/list_image_category';

$route['admin/add-category'] = 'Admin/add_category';
$route['add_category_action'] = 'Admin/add_category_action';
$route['admin/list-category'] = 'Admin/list_category';
$route['admin/delete-category/(:any)'] = 'Admin/delete_category_action/$1';
$route['admin/update-category/(:any)'] = 'Admin/update_category/$1';
$route['update_category_action'] = 'Admin/update_category_action';

$route['admin/add-class'] = 'Admin/add_schedule_class';
$route['add_class_action'] = 'Admin/add_class_action';
$route['admin/list-class/(:any)'] = 'Admin/list_classes/$1';
$route['admin/update-class/(:any)'] = 'Admin/update_schedule_class/$1';
$route['admin/delete-class/(:any)'] = 'Admin/delete_class/$1';
$route['update_class_action'] = 'Admin/update_schedule_action';

$route['home'] = 'Menu/home';
$route['about-us'] = 'Menu/about_us';
$route['services'] = 'Menu/services';
$route['classes'] = 'Menu/classes';
$route['enrollments'] = 'Menu/enrollments';
$route['enrollment_action'] = 'Mailer/enrollmentMail';
$route['appointments'] = 'Menu/appointments';
$route['appointment_action'] = 'Mailer/appointmentMail';
$route['buy-services'] = 'Menu/buy_services';
$route['service/(:any)'] = 'Services/buy_service/$1';
$route['buy_service_action'] = 'Mailer/buyServiceMail';
$route['image-gallery'] = 'Menu/image_gallery';
$route['image-gallery/(:any)'] = 'Menu/image_gallery/$1';
$route['image-gallery-view/(:any)'] = 'Menu/image_gallery_view/$1';
$route['image-gallery-view/(:any)/(:any)'] = 'Menu/image_gallery_view/$1/$2';
$route['video-gallery'] = 'Menu/video_gallery';
$route['video-gallery/(:any)'] = 'Menu/video_gallery/$1';
$route['contact-us'] = 'Menu/contact_us';
$route['contact_action'] = 'Mailer/contactMail';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
