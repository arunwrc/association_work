    <section style="margin-top:55px;margin-bottom:50px;" id="slide_show">
			<div id="slider_container_2">
				<div id="SliderName_2" class="SliderName_2">
					<img src="<?php echo base_url();?>assets/plugins/sliderman/img/1.jpg" width="700" height="450" alt="Demo2 first" title="Demo2 first" usemap="#img1map" />
					<img src="<?php echo base_url();?>assets/plugins/sliderman/img/2.jpg" width="700" height="450" alt="Demo2 second" title="Demo2 second" />
					<img src="<?php echo base_url();?>assets/plugins/sliderman/img/3.jpg" width="700" height="450" alt="Demo2 third" title="Demo2 third" />
					<img src="<?php echo base_url();?>assets/plugins/sliderman/img/4.jpg" width="700" height="450" alt="Demo2 fourth" title="Demo2 fourth" />
                    
				</div>
				<div id="SliderNameNavigation_2"></div>	
	       </div>
    </section>              
    <!-- end slider -->
    <!-- start divider -->
    <section class="home_pattern_1" id="divider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header"><span class="focued_text"><!--MIKE'S GYM--></span><!-- LATEST NEWS--></div>
                </div>    
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="page_title sub_header">WBO Boxing club welcome</div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    We know you are looking for the right arena to explore your fighting potentials. Yes, you are at the right place! But, it’s not just for those passionate boxers, beyond that it’s for every individual who is so desperate about his/her shape and poor stamina. Are you regular gym freak with more effort and zero result? It’s time to experience the boxing exercise and carve the real you. A bit dedication and timeless hard work, that’s all you need and leave the rest to us. 
                    Welcome to the realm of boxing, The WBO Boxing Club! 
                    <div class="read_more_holder">
<!--                        <a class="read_more" href="#">read more</a>-->
                    </div>    
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="page_title sub_header">Boxing</div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    Boxing involves so much of body mechanics including footwork, core mobility and blazing hand speed, being one of the biggest self-defense methods all time. Beyond defense, it's also a vigorous work out to retain physical shape and stamina. You would end up sweating easily burning loads of calories than you spend hours in gym with more effort and less result. Taught by the world class champions you will see the best of boxing here.
                    It all starts with drills and ground exercises to enhance your footwork and defending skills. Later, you get trained under our experienced boxing professionals and where you will be guided through the sparring session which allows fast progression. If you are skilled enough and have 100% dedication you would come out in flying colors.
                    <div class="read_more_holder">
<!--                        <a class="read_more" href="#">read more</a>-->
                    </div>    
                    </div>  
                </div>

            </div>
        </div>
    </section>
    <!-- end divider -->
    <!-- start feature -->
    <section class="home_pattern_2" id="feature">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title main_header"><span class="focued_text">WBO SCHEDULE</span></div><br>                    
                    <div id="monday" class="three_column_description_style2">
<!--                    In the gym there are several at lessons and activities, below you will find the daily schedule of the lessons at Mike's Gym. Are you very busy and it is hard to adapt your own schedule to the group lessons then there is always a Possibility for private lessons or one of our certified trainers. For more information regarding etc. the schedule or private lessons please contact us or just come by-->
                    </div>
                    
                    
                    <div class="schedule_list">
                        <table class="table table-hover schedule_table">
                            <thead>
                              <tr>
                                <th>WEEKLY SCHEDULE</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_monday">MONDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_tuesday">TUESDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_wednesday">WEDNESDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_thursday">THURSDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_friday">FRIDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_saturday">SATURDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item nav_to_classes" id="_sunday">SUNDAY<span class="chevron_right">></span></td>
                              </tr>    
                            </tbody>
                          </table>
                          <div>
                              <a href="#" class="nav_to_classes btn btn-info btn-xs btn-view-all-schedule" role="button">FULL SCHEDULE</a>
                          </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title main_header"><span class="focued_text">Fitness & Cardio&nbsp;</span></div><br>
                    <div class="three_column_description_style2">
                        Cardio and Fitness plays a vital role in Boxing. It’s essential to have a proper diet, cardio and fitness regime to bring out the best in you. Unfortunately, many people still don't know how to schedule an effective cardio for muscle gains, improved fitness and stamina. We focus on that part, first of all we want you to learn the benefits of cardio and how that impacts you both mentally and physically. Cardio workouts include walking, running, cycling, swimming, rowing and other aerobic workouts leading to moderate sweating. Once you get hands on, you will reap the benefits of it under an experienced trainer. A successful cardio routine involves some major elements and that will literally heighten your work out life.
                        <div class="read_more_holder">
                            <a class="read_more" href="#">read more</a>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title main_header"><span class="focued_text">Kick Boxing&nbsp;</span></div><br>
                    <div class="three_column_description_style2">
                        Fight to fit, that’s the motive of ‘Kick Boxing’. Kickboxing is an aerobic workout, which enhances the heart rate and helps to burn fat and calories. During a one -hour workout, you can expect to burn as many as 350 to 450 calories approximately. 

                        Punches built with precision and power in kickboxing help to build upper- and lower-body muscles. Core strength is improved since you balance to perform the daily routines. Finally, heart rate and blood circulation speed up, strengthening cardiovascular fitness. Since you jump, weave and bob through kicks and punches, your heart rate remains elevated. This is a total body workout thus you save time by covering cardiovascular and respiratory fitness, flexibility, strength building, endurance and body composition. Kicking and punching are good self-defense skills that develop when you learn kickboxing. 
                        <div class="read_more_holder">
                            <a class="read_more" href="#">read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end feature -->
    <!-- start divider -->
<!--
    <section class="home_pattern_1" id="video_divider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header"><span class="focued_text">Latest &nbsp;</span> WBO's Video</div>
                </div>    
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header">Video1</div>

                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video2</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video3</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video4</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video5</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video6</div>
                </div>

            </div>
        </div>
    </section>
-->
    <!-- end divider -->
    <!-- start contact -->
    <section id="contact">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                        <h2 class="text-uppercase">Contact Us</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
                        <address>
                            <p><i class="fa fa-map-marker"></i>36 Street Name, City Name, United States</p>
                            <p><i class="fa fa-phone"></i> 010-010-0110 or 020-020-0220</p>
                            <p><i class="fa fa-envelope-o"></i> info@company.com</p>
                        </address>
                    </div>
                    <div class="col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                        <div class="contact-form">
                            <form action="#" method="post">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" placeholder="Subject">
                                </div>
                                <div class="col-md-12">
                                    <textarea class="form-control" placeholder="Message" rows="4"></textarea>
                                </div>
                                <div class="col-md-8">
                                    <input type="submit" class="form-control text-uppercase" value="Send">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end contact -->
<script src="<?php echo base_url();?>assets/plugins/sliderman/js/sliderman.1.3.8.js"></script>
<script type="text/javascript">
/* Slider styles*/
effectsDemo2 = 'rain';
var demoSlider_2 = Sliderman.slider({container: 'SliderName_2', width: '100%', height: 350, effects: effectsDemo2,
    display: {
        autoplay: 3000,
        loading: {background: '#000000', opacity: 0.5, image: '/assets/plugins/sliderman/img/loading.gif'},
        buttons: {hide: false, opacity: 1, prev: {className: 'SliderNamePrev_2', label: ''}, next: {className: 'SliderNameNext_2', label: ''}},
        description: {hide: true, background: '#000000', opacity: 0.4, height: 50, position: 'bottom'},
        navigation: {container: 'SliderNameNavigation_2', label: '<img src="/assets/plugins/sliderman/img/clear.gif" />'}
    }
});
</script>