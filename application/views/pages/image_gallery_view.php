<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/folio-gallery/folio-gallery.css" />

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/plugins/colorbox/colorbox.css" />
<!--<link rel="stylesheet" type="text/css" href="fancybox/fancybox.css" />-->
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/colorbox/jquery.colorbox-min.js"></script>



    <!-- start divider -->
    <section class="home_pattern_1 about_us_page gallery_page" id="divider">    
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header"><span class="focued_text">WBO</span> image gallery</div>
                </div> 
               <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin_wrapper">
                        <?php foreach($image_lists as $image_list) { ?>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 video_gallery">
								
							</br>
							<a href=<?php   echo base_url().'admin_assets/images/'.$image_list->image_url;?> data-lightbox="roadtrip"><img src = "<?php   echo base_url().'admin_assets/images/'.$image_list->image_url;?>"></a>
							
								
                              <!--  <a href=<?php   
                                echo base_url().'admin/update-image/'.$image_list->id;?> >
                                <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <!-- -->
                               <!-- <a href="#" data-href="<?php echo base_url().'admin/delete-image/'.$image_list->id;?>" data-toggle="modal" data-target="#confirm-delete">
                                <i class="fa fa-trash fa-lg" aria-hidden="true"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <!-- -->
                                 <div class="link-top">
                                 <i class="link"> </i>
                                 </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
					<?php echo $links; ?>
            </div>
        </div>
    </section>
    <!-- end divider -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBJ3l14ukKWcVToBhR5pFj4puLQw9oojzw&sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>
<script type="text/javascript">
$(document).ready(function() {	
	
	// colorbox settings
	$(".albumpix").colorbox({rel:'albumpix'});
	
	// fancy box settings
	/*
	$("a.albumpix").fancybox({
		'autoScale	'		: true, 
		'hideOnOverlayClick': true,
		'hideOnContentClick': true
	});
	*/
});
</script>
<script type="text/javascript">
   function initialize_map() {

     var options = {
      types: ['(cities)'],
      componentRestrictions: {country: "in"}
     };
     
     var input = document.getElementById('contact_district');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
     google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();         
         var geo_location = place.formatted_address.split(",");
         var district = place.name;
         var state = geo_location['1'];
         $("#contact_district").val(district);
         $("#contact_state").val(state);
    }); 
       
    }
</script>