
    <!-- start divider -->
    <section class="home_pattern_1 about_us_page xtra_h" id="divider">    
        <div class="container">
            <div class="row">
              <form class="form-horizontal" id="buy_service_form" method="post" action="<?php echo base_url();?>buy_service_action">    
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> 
                        <div class="service_prdct">
                            <div class="detail_product_heading">
                               MIKE'S GYM RASH GUARD YELLOW
                            </div>
                            <img src="/assets/images/rashguard.png">
                            <div class="product_attributes">
                                <span class="product_amount">€ 39.95</span>
                                <span class="product_button">
                                <span class="quantity_selector">Select Quantity </span> 
                                <select name="product_quantity" class="product_quantity" id="product_quantity">
                                    <option>1</option>    
                                    <option>2</option>    
                                    <option>3</option>    
                                    <option>4</option>    
                                    <option>5</option>    
                                </select>
                                </span>
                            </div>
                        </div>
                      
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line_left_portion">
                        <div class="page_title main_header">Details</div>
                    </div> 
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="email">First Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Last Name</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Mobile</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Enter mobile">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Address</label>
                        <div class="col-sm-10"> 
                          <textarea class="address" name="address" placeholder="Enter address"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Product Name</label>
                        <div class="col-sm-10"> 

                        <input readonly  type="text" class="form-control" id="product_name" name="product_name" placeholder="" value="MIKE'S GYM RASH GUARD YELLOW">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Unit Price</label>
                        <div class="col-sm-10"> 

                        <input readonly  type="text" class="form-control" id="product_price" name="product_price" placeholder="" value="39.95">
                        </div>
                      </div>      
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Email ID</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="email_id" name="email_id" placeholder="Enter email">
                        </div>
                      </div>    
                      <div class="form-group">
                        <div class="col-sm-2"></div>  
                        <div class="col-sm-2">
                          <button type="submit" class="reset_btn btn btn-view-all-schedule">BUY NOW</button>
                        </div>
                        <div class="col-sm-8">
                          <button type="submit" class="btn btn-view-all-schedule reset_service_form reset_btn">RESET DATA</button>
                        </div>  
                      </div>
                        </div>
                         </div>
                </form>
                
            </div>
        </div>
    </section>
    <!-- end divider -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBJ3l14ukKWcVToBhR5pFj4puLQw9oojzw&sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>        
<script type="text/javascript">
   function initialize_map() {

     var options = {
      types: ['(cities)'],
      componentRestrictions: {country: "in"}
     };
     
     var input = document.getElementById('contact_district');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
     google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();         
         var geo_location = place.formatted_address.split(",");
         var district = place.name;
         var state = geo_location['1'];
         $("#contact_district").val(district);
         $("#contact_state").val(state);
    }); 
       
    }
</script>