
    <!-- start divider -->
    <section class="home_pattern_1 about_us_page xtra_h" id="divider">    
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div style="padding-left:0px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line_left_portion">
                        <div class="page_title main_header">Featured Products</div>
                    </div> 
                        <div class="service_prdct">
                            <div class="product_heading">
                                <i class="fa fa-chevron-right yellow_chevron" aria-hidden="true"></i>&nbsp;&nbsp;MIKE'S GYM RASH GUARD YELLOW
                            </div>
                            <img src="/assets/images/rashguard.png">
                            <div class="product_attributes">
                                <span class="product_amount">€ 39.95</span>
                                <span class="product_button"><a href="<?php echo base_url();?>service/2" class="nav_to_classes btn btn-info btn-xs btn-buy-service" role="button">ORDER</a></span>
                            </div>
                        </div>
                      
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <!-- -->
                            <div class="page_title main_header"><span class="focued_text">WBO SCHEDULE</span></div><br>                    
                            <div id="monday" class="three_column_description_style2">
                            In the gym there are several at lessons and activities, below you will find the daily schedule of the lessons at Mike's Gym. Are you very busy and it is hard to adapt your own schedule to the group lessons then there is always a Possibility for private lessons or one of our certified trainers. For more information regarding etc. the schedule or private lessons please contact us or just come by
                            </div>


                            <div class="schedule_list">
                            <table class="table table-hover schedule_table">
                                <thead>
                                  <tr>
                                    <th>WEEKLY SCHEDULE</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_monday">MONDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_tuesday">TUESDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_wednesday">WEDNESDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_thursday">THURSDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_friday">FRIDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_saturday">SATURDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_sunday">SUNDAY<span class="chevron_right">></span></td>
                                  </tr>    
                                </tbody>
                              </table>
                              <div>
                                  <a href="#" class="nav_to_classes btn btn-info btn-xs btn-view-all-schedule" role="button">FULL SCHEDULE</a>
                              </div>
                            </div>
                        <!-- -->
                    </div>
                         </div>
               
                
            </div>
        </div>
    </section>
    <!-- end divider -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBJ3l14ukKWcVToBhR5pFj4puLQw9oojzw&sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>        
<script type="text/javascript">
   function initialize_map() {

     var options = {
      types: ['(cities)'],
      componentRestrictions: {country: "in"}
     };
     
     var input = document.getElementById('contact_district');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
     google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();         
         var geo_location = place.formatted_address.split(",");
         var district = place.name;
         var state = geo_location['1'];
         $("#contact_district").val(district);
         $("#contact_state").val(state);
    }); 
       
    }
</script>