    <!-- start divider -->
    <section class="home_pattern_1 about_us_page" id="divider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header"><span class="focued_text">About Us</span> <!--WBO Boxing club--></div>
                </div>  
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<!--                    <div class="page_title sub_header">About us</div><br>-->
                    <div class="three_column_description">
                        The World Boxing Organization (WBO) is a sanctioning organization which recognizes professional boxing world champions. It is recognized by the International Boxing Hall of Fame (IBHOF) as one of the four major world championship groups, alongside the World Boxing Association (WBA), World Boxing Council (WBC), and International Boxing Federation (IBF). The WBO's headquarters are located in San Juan, Puerto Rico.

                        We feel proud enough to say that WBO Boxing Club is a licensed initiative approved by World Boxing Organization!

                        WBO Club literally concentrates on your fitness beyond the boxing capabilities you hold. When you hear the word ‘fitness’ you think about physical fitness, but it’s not just physical, indeed it’s a state where you are awake mentally also. We serve the best fitness regime through this initiative to all those fitness freaks irrespective of age and sex, starting from kids to elderly people. Let’s make a deep dive into WBO Boxing Club:

                    </div>
                    <div class="three_column_description">
                        •	WBO Boxing Club has the most technically advanced and highly equipped boxing machines of the nation. <br><br>
                        •	A good line up of world class punching bags and boxing accessories. <br><br>
                        •	You can watch your strike count’s power accuracy on screen live. <br><br>
                        •	We offer international trainers and fitness regime advised by them. <br><br>
                        •	You can pick personal fitness trainers for each age group. <br><br>
                        •	At the end, you can sip some delicious and healthy mocktail at our mocktail club.
                    </div>
                </div>
<!--
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title sub_header"></div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    Welcome to the official Mike’s gym website. Mike’s gym is known worldwide as the authority of kickboxing, boxing and M.M.A. It’s the home of several champions like Badr Hari, Gokhan Saki, Alistair Overeem, Murthel Groenhart, Melvin Manhoef, Artur Kyshenko, Joerie Mes, Bjorn Bregy and many more. We are not only a gym for champions and we are open for all types of people that want to experience the training, vision of Mike and his trainers.  The gym is accessible for everyone so if you are just someone that like to work out or you have to fight for a world title to fight Mike’s gym is the spot that can help you achieve your goals!
                    <div class="read_more_holder"><a class="read_more" href="#">read more</a></div>    
                    </div>
                </div>
-->
<!--
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title sub_header"></div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    Welcome to the official Mike’s gym website. Mike’s gym is known worldwide as the authority of kickboxing, boxing and M.M.A. It’s the home of several champions like Badr Hari, Gokhan Saki, Alistair Overeem, Murthel Groenhart, Melvin Manhoef, Artur Kyshenko, Joerie Mes, Bjorn Bregy and many more. We are not only a gym for champions and we are open for all types of people that want to experience the training, vision of Mike and his trainers.  The gym is accessible for everyone so if you are just someone that like to work out or you have to fight for a world title to fight Mike’s gym is the spot that can help you achieve your goals!
                    <div class="read_more_holder"><a class="read_more" href="#">read more</a></div>    
                    </div>  
                </div>
-->
<!--
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title sub_header"></div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    Welcome to the official Mike’s gym website. Mike’s gym is known worldwide as the authority of kickboxing, boxing and M.M.A. It’s the home of several champions like Badr Hari, Gokhan Saki, Alistair Overeem, Murthel Groenhart, Melvin Manhoef, Artur Kyshenko, Joerie Mes, Bjorn Bregy and many more. We are not only a gym for champions and we are open for all types of people that want to experience the training, vision of Mike and his trainers.  The gym is accessible for everyone so if you are just someone that like to work out or you have to fight for a world title to fight Mike’s gym is the spot that can help you achieve your goals!
                    <div class="read_more_holder"><a class="read_more" href="#">read more</a></div>    
                    </div>  
                </div>
-->

            </div>
        </div>
    </section>
    <!-- end divider -->
    <!-- start divider -->