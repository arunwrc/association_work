
    <!-- start divider -->
    <section class="home_pattern_1 about_us_page" id="divider">    
        <div class="container">
            <div class="row">
<!--
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header">Welcome to Enrollment</div>
                </div>
-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line_left_portion">
                    <div class="page_title main_header">Welcome to WBO Boxing Club Enrollment</div>
                </div>    
                <form class="form-horizontal" id="enrollment_form" method="post" action="<?php echo base_url();?>enrollment_action">
                        <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Membership:</label>
                    
                        <div style="margin-bottom:10px;" class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                            <div style="text-align:left;color:#ff0000;font-size:16px;font-weight:600;font-family:'Lato';"> <span style="display:inline-block;width:70px;margin-top:7px;" id="selected_membership">Platinum</span></div><br>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <label>
                                <input type="radio" name="member_type" value="Platinum" checked>
                                <img src="/assets/images/bronze.png">
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <label>
                                <input type="radio" name="member_type" value="Gold">
                                <img src="/assets/images/bronze.png">
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <label>
                                <input type="radio" name="member_type" value="Silver">
                                <img src="/assets/images/bronze.png">
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <label>
                                <input type="radio" name="member_type" value="Bronze">
                                <img src="/assets/images/bronze.png">
                                </label>
                            </div>

                        </div>
                    </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="email">First Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Last Name</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">DOB</label>
                        <div class="col-sm-10"> 
                            <div class="input-group input-append date">
                                <input type="text" class="form-control" name="date" id="dob" />
                                <span class="input-group-addon add-on"></span>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Mobile</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Enter mobile">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Address</label>
                        <div class="col-sm-10"> 
                          <textarea class="address" name="address" placeholder="Enter address"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">District</label>
                        <div class="col-sm-10"> 
                        <input onkeyup="initialize()" type="text" class="form-control" id="district" name="district" placeholder="Enter district">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">State</label>
                        <div class="col-sm-10"> 
                        <input readonly  type="text" class="form-control" id="state" name="state" placeholder="Enter state">
                        </div>
                      </div>    
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Email ID</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="email_id" name="email_id" placeholder="Enter email">
                        </div>
                      </div>    
                      <div class="form-group">
                        <div class="col-sm-2"></div>  
                        <div class="col-sm-2">
                          <button type="submit" class="btn btn-view-all-schedule">ENROLL NOW</button>
                        </div>
                        <div class="col-sm-8">
                          <button type="submit" class="reset_btn btn btn-view-all-schedule reset_enrollment_form">RESET DATA</button>
                        </div>  
                      </div>
                    </form>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <!-- -->
                            <div class="page_title main_header">ENROLLMENT</div><br>                    
                            <div class="three_column_description_style2">
                            This program has been planned, designed and developed with reputed boxing personalities across the globe to bring the best shape and stamina in you. It takes so much of dedication, interest, willingness, courage and countless hours of hard work to become a passionate boxer. Thousands of people are drooling over a perfectly toned body embraced with great stamina and why don’t you grab your turn? Please fill in this form and get registered with one of the most reputed Boxing Club of the nation.
                            </div>
                        
<!--
                            <div class="page_title main_header"><span class="focued_text">WBO SCHEDULE</span></div><br>                    
                            <div id="monday" class="three_column_description_style2">
                            In the gym there are several at lessons and activities, below you will find the daily schedule of the lessons at Mike's Gym. Are you very busy and it is hard to adapt your own schedule to the group lessons then there is always a Possibility for private lessons or one of our certified trainers. For more information regarding etc. the schedule or private lessons please contact us or just come by
                            </div>
-->


                            <div class="schedule_list">
                            <table class="table table-hover schedule_table">
                                <thead>
                                  <tr>
                                    <th>WEEKLY SCHEDULE</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_monday">MONDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_tuesday">TUESDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_wednesday">WEDNESDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_thursday">THURSDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_friday">FRIDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_saturday">SATURDAY<span class="chevron_right">></span></td>
                                  </tr>
                                  <tr>
                                    <td class="schedule_item nav_to_classes" id="_sunday">SUNDAY<span class="chevron_right">></span></td>
                                  </tr>    
                                </tbody>
                              </table>
                              <div>
                                  <a href="#" class="nav_to_classes btn btn-info btn-xs btn-view-all-schedule" role="button">FULL SCHEDULE</a>
                              </div>
                            </div>
                        <!-- -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end divider -->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBJ3l14ukKWcVToBhR5pFj4puLQw9oojzw&sensor=false&libraries=places&language=en-AU"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
       'version':'1','packages':['timeline']}]}"></script>        
<script type="text/javascript">
   function initialize() {

     var options = {
      types: ['(cities)'],
      componentRestrictions: {country: "in"}
     };
     
     var input = document.getElementById('district');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
     google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();         
         var geo_location = place.formatted_address.split(",");
         var district = place.name;
         var state = geo_location['1'];
         $("#district").val(district);
         $("#state").val(state);
    }); 
       
    }
</script>