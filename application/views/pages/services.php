    <!-- start divider -->
    <section class="home_pattern_1 about_us_page" id="divider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header"><span class="focued_text">Welcome to</span> WBO Boxing club</div>
                </div>    
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="page_title sub_header">GROUP BOXING</div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    Group fitness boxing workouts have been extensively advised to young and middle aged people to perpetuate their stamina and to restrain their health. The fitness rounds involved in this session appears to be bit tough comparing to the fitness sessions practiced by kids and aged people. You will be accompanied with professional trainers through each session to bringing the best in you, both in terms of fitness and health. Group Boxing is much more fun involved, since you are playing in a team with a bunch of fitness enthusiastic people!
                    <div class="read_more_holder"><!--<a class="read_more" href="#">read more</a>--></div>  
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="page_title sub_header">KIDS & AGED PEOPLE</div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    At WBO Boxing Club, we aim to elevate your kid’s physical stamina, resistance to illness and mental fitness through the vigorous fitness sessions we provide under certified professionals.

                    Our fitness training includes jumping rope, jogging and sparring to enhance your child's cardiovascular fitness. We indeed focus to strengthening your child’s arms and legs. Apart from this, bodyweight exercises such as push-ups, crunches and squats are highly recommended. These fitness sessions helps your child to gain
                    more endurance alongside great physical shape.

                    There are ample reasons to choose boxing fitness for aged people. Boxing fitness helps to increase your bone density and make you more capable. As we age, we tend to lose the stability of our muscles thus revealing our real age. Through a very controlled fitness regime you call recall your youngness and sex appeal very easily both in terms of look and strength. If you just don’t get enough with the kids, you’ll find some at the gym. They will be faster and have a higher punch count and they will bring a serious press to you in the ring, but you’ll be able to outlast and out think them. If you take boxing fitness seriously, you’re going to want to support it every way you can, in the sense you can get rid of all the excess fat and cholesterol in addition you can start having the food of your favorite choice too. 
    
                    <div class="read_more_holder"><!--<a class="read_more" href="#">read more</a>--></div>    
                    </div>  
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="page_title sub_header">BOXING MERCHANDISE </div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    We are having a dedicated team to build exclusive ‘WBO Boxing Club’ merchandise for our members. You will find all essential protective and support gears required to attend any form of vigorous workout sessions. We produce the most durable and highly reliable products assuring your safety and comfort using high quality leather, cloth, rubber, felted wool thus making it the finest fitness gears available in the market.
                   <div class="read_more_holder"><!--<a class="read_more" href="#">read more</a>--></div> 
                    </div>  
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="page_title sub_header">MOCKTAIL CLUB</div><br>
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="three_column_description">
                    We don’t just stop with providing fitness to you. We know you are too exhausted once the session is done and we don’t want anyone to leave here worn out and that’s the idea behind our Mocktail club.
                    Having a drink or few with your friends or mate is always fun and relaxing. The full-fledged Mocktail club we have certainly aims at your personal preference or for health. There are far too many fun, exotic and sexy non-alcoholic alternatives for you to enjoy! We insist you to have concentrated or synthetic energy drinks after heavy boxing fitness sessions. Most of the fruits mock tails can gain you required energy with the same taste of natural fresh fruits.
                    Today, with the elaborate range of ingredients available we regularly craft the best cocktail infused syrups and fresh-squeezed fruit and vegetable juices to scented waters, bitters and fresh herbs which are healthy and will stay as a companion once you finish your sessions.
    
                    <div class="read_more_holder"><!--<a class="read_more" href="#"><!--read more--></a></div>  
                    </div>  
                </div>

            </div>
        </div>
    </section>
    <!-- end divider -->