    <!-- start divider -->
    <section class="home_pattern_1 about_us_page" id="divider">
        <?php 
//                    echo "<pre>";
//                    print_r($class_list);
//                    echo "<pre>";
                    ?>
        <div class="container">
            <div class="row">  
            
                <div  class="col-lg-8 col-md-8 col-sm-12 col-xs-12 schedule_wrapper">
                    <div class="page_title subpage_main_header title_under_line">SCHEDULE WEEK SCHEDULE</div>
                    
                    <?php 
                    foreach($class_list as $key => $class){
                    ?>
                    <div id="<?php echo strtolower($class -> name);?>" class="schedule_column_description">
                    <span class="scheduled_day"><?php echo strtoupper($class -> name);?>:</span><br><br>    
                    
                        
                        <?php foreach($class->informedTo as $informedto){ ?>
                        <div class="scheduled_data">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i><?php echo $informedto->class_name;?>
                        <span class="scheduled_time">
                            <?php echo $informedto->from_time;
                            if($informedto->to_time){
                                echo " TO ".$informedto->to_time;
                            }
                            ?>
                        </span>     
                        </div>    
                        <?php }?>    
                   
                    </div>
                    <?php } ?>
                    
                    
<!--
                    <div id="monday" class="schedule_column_description">
                    <span class="scheduled_day">MONDAY:</span><br><br>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> FITNESS
                       <span class="scheduled_time">06:30 TO 22:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">09:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BRAZILIAN JIU JITSU (ADVANCED)
                       <span class="scheduled_time">10:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> YOUTH KICKBOXING 4 TO 9 YEARS
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KETTLEBELL PROF
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BOX
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>       
                    </div>
                    
                    <div id="tuesday" class="schedule_column_description">
                    <span class="scheduled_day">TUESDAY:</span><br><br>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> FITNESS
                       <span class="scheduled_time">22:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i>  MMA
                       <span class="scheduled_time">09:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BKETTLEBELL PROF
                       <span class="scheduled_time">10:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BRAZILIAN JIU JITSU (ADVANCED)
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BOX
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    </div>
                    <div id="wednesday" class="schedule_column_description">
                    <span class="scheduled_day">WEDNESDAY:</span><br><br>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> FITNESS
                       <span class="scheduled_time">06:30 TO 22:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    </div>
                    <div id="thursday" class="schedule_column_description">
                    <span class="scheduled_day">THURSDAY:</span><br><br>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> FITNESS
                       <span class="scheduled_time">06:30 TO 22:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">09:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BRAZILIAN JIU JITSU (ADVANCED)
                       <span class="scheduled_time">10:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> YOUTH KICKBOXING 4 TO 9 YEARS
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KETTLEBELL PROF
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BOX
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    </div>
                    <div id="friday" class="schedule_column_description">
                    <span class="scheduled_day">FRIDAY:</span><br><br>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BOX
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    </div>
                    <div id="saturday" class="schedule_column_description">
                    <span class="scheduled_day">SATURDAY:</span><br><br>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> YOUTH KICKBOXING 4 TO 9 YEARS
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KETTLEBELL PROF
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">18:00</span>  
                    </div>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> FITNESS
                       <span class="scheduled_time">06:30 TO 22:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">09:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BRAZILIAN JIU JITSU (ADVANCED)
                       <span class="scheduled_time">10:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> YOUTH KICKBOXING 4 TO 9 YEARS
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KETTLEBELL PROF
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BOX
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    </div>
                    <div id="sunday" class="schedule_column_description">
                    <span class="scheduled_day">SUNDAY:</span><br><br>  
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">09:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BRAZILIAN JIU JITSU (ADVANCED)
                       <span class="scheduled_time">10:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> YOUTH KICKBOXING 4 TO 9 YEARS
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KETTLEBELL PROF
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>    
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> FITNESS
                       <span class="scheduled_time">06:30 TO 22:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">09:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BRAZILIAN JIU JITSU (ADVANCED)
                       <span class="scheduled_time">10:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> YOUTH KICKBOXING 4 TO 9 YEARS
                       <span class="scheduled_time">16:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KETTLEBELL PROF
                       <span class="scheduled_time">17:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING YOUTH 9 TO 13 YEARS
                       <span class="scheduled_time">17:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> LADIES KICKBOXING
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BOX
                       <span class="scheduled_time">18:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BEGINNERS KICKBOXIN
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> KICKBOXING MATCH
                       <span class="scheduled_time">19:00 TO 20:30</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> MMA
                       <span class="scheduled_time">19:00</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> TAI-BO
                       <span class="scheduled_time">19:00</span>  
                    </div> 
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> BAG TRAINING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    <div class="scheduled_data">
                       <i class="fa fa-chevron-right" aria-hidden="true"></i> HARVEY KICKBOXING
                       <span class="scheduled_time">08:00 PM</span>  
                    </div>
                    </div>
                    
-->
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="page_title subpage_main_header title_under_line">CLASSES</div>
                    
                    <div class="three_column_description">
                    There are several compulsory lessons and activities which you are supposed to follow on a regular basis. Here, you will find the daily schedule of the lessons at WBO Boxing Club. If you have a tight schedule, you can indeed take a personal training course under any of our certified trainer. You will be guided throughout the session under our dedicated training team. We request you to follow your regular schedules without fail for better results. We kindly request you to attend the classes allotted to you.
                    </div>
                    
                    
<!--
                    <div class="page_title subpage_main_header title_under_line">WBO SCHEDULE</div>
                    
                    <div id="monday" class="three_column_description">
                    In the gym there are several at lessons and activities, below you will find the daily schedule of the lessons at Mike's Gym. Are you very busy and it is hard to adapt your own schedule to the group lessons then there is always a Possibility for private lessons or one of our certified trainers. For more information regarding etc. the schedule or private lessons please contact us or just come by
                    </div>
-->
                    
                    
                    <div class="schedule_list">
                        <table class="table table-hover schedule_table">
                            <thead>
                              <tr>
                                <th>WEEKLY SCHEDULE</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="schedule_item" id="_monday">MONDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item" id="_tuesday">TUESDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item" id="_wednesday">WEDNESDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item" id="_thursday">THURSDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item" id="_friday">FRIDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item" id="_saturday">SATURDAY<span class="chevron_right">></span></td>
                              </tr>
                              <tr>
                                <td class="schedule_item" id="_sunday">SUNDAY<span class="chevron_right">></span></td>
                              </tr>    
                            </tbody>
                          </table>
                          <div>
                              <a href="#" class="btn btn-info btn-xs btn-view-all-schedule" role="button">FULL SCHEDULE</a>
                          </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- end divider -->
    <!-- start divider -->
    <section class="home_pattern_1" id="video_divider">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 title_under_line">
                    <div class="page_title main_header"><span class="focued_text">Latest &nbsp;</span> WBO's Video</div>
                </div>    
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header">Video1</div>

                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video2</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video3</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video4</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video5</div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="image_holder">
                    <img src="assets/images/jim.jpeg">
                    </div>
                    <div class="page_title sub_header video_header"> Video6</div>
                </div>

            </div>
        </div>
    </section>
    <!-- end divider -->
