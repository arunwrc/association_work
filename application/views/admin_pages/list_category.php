<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">    
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        CONFIRM DELETE
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this image?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <?php
                if(isset($_SESSION['success'])){
                    echo "<div class='alert alert-success'>
                    <strong>Success! </strong>" .$_SESSION['success']."
                    </div>";   
                }
                if(isset($_SESSION['failure'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['failure']."
                    </div>";   
                }
                if(isset($_SESSION['already_updated'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['already_updated']."
                    </div>";   
                }    
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $Heading;?> <small></small>
                        </h1>
<!--
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $Heading;?>
                            </li>
                        </ol>
						
-->

					<table class="table table-striped" id="newss-tbl">
                            <thead>
                                <tr>
                                    <th>Sl no</th>
                                    <th>Category</th>
                                    
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $slno=1;foreach ($category as $categorys) { ?>
                                <tr>
                                    <td><?php echo $slno++;?></td>
                                    <td><?php echo $categorys->name;?></td>
									<td><a  href="<?php echo base_url().'admin/update-category/'.$categorys->id;?>"><i class="fa fa-edit"></i></a></td>
                                    <td><a  href="<?php echo base_url().'admin/delete-category/'.$categorys->id;?>"><i class="fa fa-trash-o"></i></a></td>
									
                                </tr>
                                <?php  }?>
                                
                            </tbody>
                        </table>
                   
                    </div>
                </div>
                <!-- /.row -->
					
                
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

 <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
