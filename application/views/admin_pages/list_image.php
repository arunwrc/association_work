        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        CONFIRM DELETE
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this image?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <?php
                if(isset($_SESSION['success'])){
                    echo "<div class='alert alert-success'>
                    <strong>Success! </strong>" .$_SESSION['success']."
                    </div>";   
                }
                if(isset($_SESSION['failure'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['failure']."
                    </div>";   
                }
                if(isset($_SESSION['already_updated'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['already_updated']."
                    </div>";   
                }    
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $Heading;?> <small></small>
                        </h1>
<!--
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $Heading;?>
                            </li>
                        </ol>
-->
                    </div>
                </div>
                <!-- /.row -->
				<button type="button" id="add-image" class="btn btn-primary insert_data">Add Image</button></br></br>
				<div id="add-image-div">
				
						<form  method="post" action="<?php echo base_url()?>add_image_action" enctype="multipart/form-data">

                         <input name="image_category_id" type="hidden" value="<?php echo $image_lists[0]->image_category_id;?>">
                   
                            <div class="form-group">
								
                                <label>Add Image</label>
                                <input type="file" name="userfile[]" multiple>
                           
                            </div>
                            <button type="submit" class="btn btn-primary insert_data">Insert Data</button>
							</br></br>
                        </form>
				</div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin_wrapper">
                        <?php foreach($image_lists as $image_list) { ?>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 video_gallery">
								
								<img src= <?php   echo base_url().'admin_assets/images/'.$image_list->image_url;?> class="img-responsive" alt="" >
                                <a href=<?php   
                                echo base_url().'admin/update-image/'.$image_list->id;?> >
                                <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <!-- -->
                                <a href="#" data-href="<?php echo base_url().'admin/delete-image/'.$image_list->id;?>" data-toggle="modal" data-target="#confirm-delete">
                                <i class="fa fa-trash fa-lg" aria-hidden="true"></i>
                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <!-- -->
                                 <div class="link-top">
                                 <i class="link"> </i>
                                 </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
				<p>		<?php echo $links; ?> </p>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->


