        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <?php
                if(isset($_SESSION['success'])){
                    echo "<div class='alert alert-success'>
                    <strong>Success! </strong>" .$_SESSION['success']."
                    </div>";   
                }
                if(isset($_SESSION['failure'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['failure']."
                    </div>";   
                }
                if(isset($_SESSION['invalidEntry'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['invalidEntry']."
                    </div>";   
                }    
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $Heading;?> <small></small>
                        </h1>
<!--
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $Heading;?>
                            </li>
                        </ol>
-->
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin_wrapper">
                        <form id="add_schedule_class"  method="post" action="<?php echo base_url()?>add_class_action" >
                            <div class="form-group">
                                <label>Select class day</label>
                                <select id="class_day" name="class_day" class="form-control" type="text">
                                  <option value="">Please Select</option>    
                                <?php foreach ($class_days as $class_day) { ?>    
                                  <option value="<?php echo $class_day->id;?>">
                                      <?php echo $class_day->name;?>
                                  </option>
                                <?php }?>    
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Add Class name</label>
                                <input type="text" name="class_name" id="class_name" class="form-control">
                           
                            </div>
                            <div class="form-group">
                                <label>Add Class from</label>
                                <input type="text" name="class_from" id="class_from" class="form-control">
                           
                            </div>
                            <div class="form-group">
                                <label>Add Class to</label>
                                <input type="text" name="class_to" class="form-control">
                           
                            </div>
                            <button type="submit" class="btn btn-primary insert_data">Insert Data</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->


