        <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        CONFIRM DELETE
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this image?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger btn-ok">Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <?php
                if(isset($_SESSION['success'])){
                    echo "<div class='alert alert-success'>
                    <strong>Success! </strong>" .$_SESSION['success']."
                    </div>";   
                }
                if(isset($_SESSION['failure'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['failure']."
                    </div>";   
                }
                if(isset($_SESSION['already_updated'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['already_updated']."
                    </div>";   
                }    
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $Heading;?> <small></small>
                        </h1>
<!--
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $Heading;?>
                            </li>
                        </ol>
-->
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <form style="text-align:center;" method="get" name="class_selector">
                        <div class="form-group">
                            
                            <?php 
                            if(isset($_GET['class_day'])){
                                $id = $_GET['class_day'];
                            }else{
                                $id =1;
                            }
                            $results = $this->db->query("SELECT * FROM schedule_class_time WHERE schedule_class_id =  '$id'")->result();
                            $day = $this->db->query("SELECT * FROM schedule_class WHERE  id =  '$id'")->result();
                            ?>
                            <label>Selected day <?php echo $day[0]->name; ?></label>
                            <select style="width:10%;margin:0 auto;" id="class_day" name="class_day" class="form-control" type="text" onchange="this.form.submit()">
                            <option value="">Please select</option>    
                            <?php foreach ($class_days as $class_day) { ?>    
                              <option value="<?php echo $class_day->id;?>">
                                  <?php echo $class_day->name;?>
                              </option>
                            <?php }?>    
                            </select>
                        </div>
                    </form>
                    <table class="table table-striped" id="newss-tbl">
                        <thead>
                            <tr>
                                <th>Sl no</th>
                                <th>Class name</th>
                                <th>Class from time</th>
                                <th>Class to time</th>
                                <th colspan="2">Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php $slno=1;foreach ($results as $result) { ?>
                            <tr>
                                <td><?php echo $slno++;?></td>
                                <td><?php echo $result->class_name;?></td>
                                <td><?php echo $result->from_time;?></td>
                                <td><?php echo $result->to_time;?></td>
                                <td><a  href="<?php echo base_url().'admin/update-class/'.$result->id;?>"><i class="fa fa-edit"></i></a></td>
                                <td><a  href="<?php echo base_url().'admin/delete-class/'.$result->id;?>"><i class="fa fa-trash-o"></i></a></td>

                            </tr>
                            <?php  }?>

                        </tbody>
                    </table>
                </div>
				<p>		<?php //echo $links; ?> </p>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->


