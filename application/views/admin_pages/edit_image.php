        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <?php
                if(isset($_SESSION['success'])){
                    echo "<div class='alert alert-success'>
                    <strong>Success! </strong>" .$_SESSION['success']."
                    </div>";   
                }
                if(isset($_SESSION['failure'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['failure']."
                    </div>";   
                }
                if(isset($_SESSION['already_updated'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['already_updated']."
                    </div>";   
                }    
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $Heading;?> <small></small>
                        </h1>
<!--
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $Heading;?>
                            </li>
                        </ol>
-->
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div style="min-height:370px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form name="add_video" method="post" action="<?php echo base_url()?>update_image_action" enctype="multipart/form-data">
                            <input name="hid_id" type="hidden" value="<?php echo $image_data['id'];?>">
                            <input name="hid_name" type="hidden" value="<?php echo $image_data['image_url'];?>">
                            <div class="form-group">
							 <?php
                                echo form_dropdown('image_category_id', @$category_li, @$image_data['image_category_id'],'class="form-control" required');
                                ?>
								</br>
							<input type="file" name="userfile" >	
							</br>							
                          <img src= <?php   echo base_url().'admin_assets/images/'.$image_data['image_url'];?> class="img-responsive" width="200px" >
                            </div>
                            <button type="submit" class="btn btn-primary insert_data">Insert Data</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->


