        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <?php
                if(isset($_SESSION['success'])){
                    echo "<div class='alert alert-success'>
                    <strong>Success! </strong>" .$_SESSION['success']."
                    </div>";   
                }
                if(isset($_SESSION['failure'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['failure']."
                    </div>";   
                }
                if(isset($_SESSION['invalidEntry'])){
                    echo "<div class='alert alert-danger'>
                    <strong>Failed! </strong>" .$_SESSION['invalidEntry']."
                    </div>";   
                }    
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $Heading;?> <small></small>
                        </h1>
<!--
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> <?php echo $Heading;?>
                            </li>
                        </ol>
-->
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 admin_wrapper">
                        <form  method="post" action="<?php echo base_url()?>add_image_action" enctype="multipart/form-data">
						
                        
                   
                            <div class="form-group">
								<label class="control-label col-sm-4">Category  <span class="required"> * </span>:</label>
                          
                                <?php
                                echo form_dropdown('image_category_id', @$category_li, @$image_category_id,'class="form-control" required');
                                ?>
								</br>
								<div id="add_data_div">
                                <label>Add Image</label>
                                <input type="file" name="userfile[]" multiple>
								</div>
								 <button type="button" class="btn btn-primary insert_data" id="add_data">+</button>
								 
                            </div>
                            <button type="submit" class="btn btn-primary insert_data">Insert Data</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->


