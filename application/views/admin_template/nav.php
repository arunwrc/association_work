

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>admin">WBO Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php //echo $_SESSION['user_name'];?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url();?>/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url();?>admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#videos"><i class="fa fa-fw fa-desktop"></i> Videos <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="videos" class="collapse">
                            <li>
                                <a href="<?php echo base_url();?>admin/list-video">List Videos</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>admin/add-video">Add Video</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#images"><i class="fa fa-fw fa-table"></i> Images <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="images" class="collapse">
                            <li>
                                <a href="<?php echo base_url();?>admin/list-image-category">List Images</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>admin/add-image">Add Images</a>
                            </li>
							<li>
                                <a href="<?php echo base_url();?>admin/add-category">Add Category</a>
                            </li>
							<li>
                                <a href="<?php echo base_url();?>admin/list-category">List Category</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#classes"><i class="fa fa-fw fa-table"></i> Classes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="classes" class="collapse">
                            <li>
                                <a href="<?php echo base_url();?>admin/list-class/1">List Classes</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>admin/add-class">Add Classes</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>