<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $Title;?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="description" content="">
        <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.ico" type="image/x-icon">
		<!-- animate css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
		<!-- google font -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700,800' rel='stylesheet' type='text/css'>
		<!-- custom css -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/templatemo-style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/sliderman/css/sliderman.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<!--        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />-->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
		<link href="<?php echo base_url();?>assets/css/lightbox.css" rel="stylesheet">
        <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
	</head><?php echo "\n";?>