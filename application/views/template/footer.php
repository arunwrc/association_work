    <?php echo "\n";?>
    <!-- start footer -->
    <footer>
        <div class="container mob_container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 footer_logo_holder">
                <a class="logo_link" href="#">    
                    <img class="footer_logo" src="<?php echo base_url();?>/assets/images/logo.jpg">
                </a>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                <div class="footer_content">  
                Mike’s gym is in het bezit van een Fight Right keurmerk. Dit keurmerk waarborgt en onderstreept dat de vereniging en trainers verantwoord, pedagogisch en veilig omgaan met vechtsport. Sebastiaan Munter, van het Nederlands Instituut voor Vechtsport en Maatschappij (NIVM) overhandigde het keurmerk aan Mike Passenier en Danny Delvers. Danny is naast trainer ook combinatiefunctionaris van de Dienst Maatschappelijke Ontwikkeling van de gemeente Amsterdam. <br>
                <span class="footer_yellow_text">Algemene Voorwaarden Mike's Gym</span>
                </div>      
                </div>
            </div>
        </div>
    </footer>
    <!-- end footer -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.singlePageNav.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
	<script src="<?php echo base_url();?>assets/js/lightbox.js"></script>
	</body>
</html>