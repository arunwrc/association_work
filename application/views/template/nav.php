<?php 	
$current_url = $this->uri->segment(1);
function check_active($link,$current_url){
    if($link == $current_url){
        echo $class="active";
    }else{
        echo $class="inactive";
    }
}
?>
    <body>    
    <div class="preloader">
        <div class="sk-spinner sk-spinner-rotating-plane"></div>
     </div>
    <!-- end preloader -->
    <!-- start navigation -->
    <nav class="navbar navbar-default navbar-fixed-top templatemo-nav" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                </button>
                <div class="logo_holder">
                    <a class="logo_link" href="#"><img src="<?php echo base_url();?>/assets/images/logo.jpg"></a>
                </div>    
            </div>
            <div class="menu_holder collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right text-uppercase"> 
                    <span class="slogan">'A GENTLEMAN WILL WALK NEVER RUN !' </span><br>
                    <li class="<?php check_active("home",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>home">Home</a></li>
                    <li class="<?php check_active("about-us",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>about-us">About Us</a></li>
                    <li class="<?php check_active("services",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>services">Services</a></li>
                    <!-- -->
                    <?php 
                    $ur_segment = $this->uri->segment(1);
                    $dropdown_class="inactive";
                    if($ur_segment == "classes" || $ur_segment == "enrollments" || $ur_segment == "appointments"){
                        $dropdown_class="active";
                    }else{
                        $dropdown_class="inactive";
                    }
                    ?>
                    <li class="dropdown booking_menu <?php echo $dropdown_class;?>">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">WBO Bookings <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">				          			            
                        <li class="<?php check_active("classes",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>classes">Classes</a></li>
                        <li class="<?php check_active("enrollments",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>enrollments">Enrollments</a></li>
                        <li class="<?php check_active("appointments",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>appointments">Appointments</a></li>
                      </ul>
                    </li> 
                    <!-- -->
                    <li class="<?php check_active("buy-services",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>buy-services">Buy Services</a></li>
					 
                    <li class="dropdown booking_menu <?php echo $dropdown_class;?>">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Gallery <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">				          			            
                       
                        <li class="<?php check_active("video-gallery",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>video-gallery">Videos</a></li>
                       
						<li class="<?php check_active("image-gallery",$this->uri->segment(1))?>"><a href="<?php echo base_url();?>image-gallery">Images</a></li>
                      </ul>
                    </li> 
                    
                    <li class="<?php check_active("contact-us",$this->uri->segment(1))?>" ><a href="<?php echo base_url();?>contact-us">Contact Us</a></li>
                </ul>
            </div>
            
        </div>
    </nav>
    <!-- end navigation -->
    <?php echo "\n";?>