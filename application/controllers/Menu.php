<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {


	public function home(){
        $data['Title']="Home || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/home', $data);
        $this->load->view('template/footer');
    }
    public function about_us(){
        $data['Title']="About us || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/about_us', $data);
        $this->load->view('template/footer');
    }
    public function services(){
        $data['Title']="Services || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/services', $data);
        $this->load->view('template/footer');
    }
    public function classes(){
        $this->load->model('schedule_class_model');
        $data['Title']="Classes || Association Official Website";
        $class_list = $this->schedule_class_model->list_all();
        if($class_list){
			foreach($class_list as $key =>$value){ 
                $class_list[$key]->informedTo = $this->schedule_class_model->list_classes_by_id($value->id);
            }
        }
        $data['class_list'] = $class_list;
        
//        echo "<pre>";
//        print_r($merge_data); exit;
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/classes', $data);
        $this->load->view('template/footer');
    }
    public function appointments(){
        $data['Title']="Appointment || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/appointments', $data);
        $this->load->view('template/footer');
    }
    public function enrollments(){
        $data['Title']="Enrollments || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/enrollments', $data);
        $this->load->view('template/footer');
    }
    public function buy_services(){
        $data['Title']="Buy Services || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/buy_services', $data);
        $this->load->view('template/footer');
    }
    public function image_gallery(){
        $data['Title']="Image gallery || Association Official Website";
		 $this->load->model('media_model');
		 
		 $this->load->library('pagination');
		 $config['full_tag_open'] = "<ul class='pagination'>";
$config['full_tag_close'] ="</ul>";
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['next_tag_open'] = "<li>";
$config['next_tagl_close'] = "</li>";
$config['prev_tag_open'] = "<li>";
$config['prev_tagl_close'] = "</li>";
$config['first_tag_open'] = "<li>";
$config['first_tagl_close'] = "</li>";
$config['last_tag_open'] = "<li>";
$config['last_tagl_close'] = "</li>";
		 $config["base_url"] = base_url() . "image-gallery/";
        $config["total_rows"] = $this->media_model->record_count_category();;
        $config["per_page"] = 2;
        $config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		 $data["image_lists"] = $this->media_model->list_image_category($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		 
		 
		// $data['image_lists'] = $this->media_model->list_image_category();
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/image_gallery', $data);
        $this->load->view('template/footer');
    }
	 public function image_gallery_view(){
        $data['Title']="Image gallery || Association Official Website";
		 $this->load->model('media_model');
		  $id = $this->uri->segment(2);
		  
		$this->load->library('pagination');
		$config['full_tag_open'] = "<ul class='pagination'>";
$config['full_tag_close'] ="</ul>";
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['next_tag_open'] = "<li>";
$config['next_tagl_close'] = "</li>";
$config['prev_tag_open'] = "<li>";
$config['prev_tagl_close'] = "</li>";
$config['first_tag_open'] = "<li>";
$config['first_tagl_close'] = "</li>";
$config['last_tag_open'] = "<li>";
$config['last_tagl_close'] = "</li>";
		$config["base_url"] = base_url() . "image-gallery-view/".$id.'/';
        $config["total_rows"] = $this->media_model->record_count($id);
        $config["per_page"] = 3;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		 $data["image_lists"] = $this->media_model->list_image($config["per_page"], $page,$id);
        $data["links"] = $this->pagination->create_links();
		 
		  
		// $data['image_lists'] = $this->media_model->list_image_id($id);
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/image_gallery_view', $data);
        $this->load->view('template/footer');
    }
	 public function video_gallery(){
        $data['Title']="Image gallery || Association Official Website";
		 $this->load->model('media_model');
		 
		$this->load->library('pagination');
		$config['full_tag_open'] = "<ul class='pagination'>";
$config['full_tag_close'] ="</ul>";
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['next_tag_open'] = "<li>";
$config['next_tagl_close'] = "</li>";
$config['prev_tag_open'] = "<li>";
$config['prev_tagl_close'] = "</li>";
$config['first_tag_open'] = "<li>";
$config['first_tagl_close'] = "</li>";
$config['last_tag_open'] = "<li>";
$config['last_tagl_close'] = "</li>";
		$config["base_url"] = base_url() . "video-gallery/";
        $config["total_rows"] = $this->media_model->record_count_video();;
        $config["per_page"] = 1;
        $config["uri_segment"] = 2;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        $this->pagination->initialize($config);
		
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		
		 $data["video_lists"] = $this->media_model->list_video(@$config["per_page"], @$page);
        $data["links"] = $this->pagination->create_links();
		 
		 
		 
		// $data['video_lists'] = $this->media_model->list_video();
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/video_gallery', $data);
        $this->load->view('template/footer');
    }
    public function contact_us(){
        $data['Title']="Contact Us || Association Official Website";
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/contact', $data);
        $this->load->view('template/footer');
    }
    
    
}


