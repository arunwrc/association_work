<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailer extends CI_Controller {


	public function enrollmentMail(){
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => USERNAME, // FROM CONSTANTS
        'smtp_pass' => PASSWORD, // FROM CONSTANTS
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($_POST['email_id'], $_POST['first_name']); // change it to yours
        $this->email->to(RECEIPIENT_EMAIL);// change it to yours
        $this->email->subject('Enrollment details');
        $mail_body = $this->load->view('email_template/enrollment_mail_body', $_POST,TRUE);
        $this->email->message($mail_body);
        if($this->email->send()){
            redirect(base_url().'home', 'refresh');
        }
        else{
            show_error($this->email->print_debugger());
        }

    }
    public function appointmentMail(){
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => USERNAME, // FROM CONSTANTS
        'smtp_pass' => PASSWORD, // FROM CONSTANTS
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($_POST['email_id'], $_POST['first_name']); // change it to yours
        $this->email->to(RECEIPIENT_EMAIL);// change it to yours
        $this->email->subject('Appointment details');
        $mail_body = $this->load->view('email_template/appointment_mail_body', $_POST,TRUE);
        $this->email->message($mail_body);
        if($this->email->send()){
            redirect(base_url().'home', 'refresh');
        }
        else{
            show_error($this->email->print_debugger());
        }

    }
    public function buyServiceMail(){
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => USERNAME, // FROM CONSTANTS
        'smtp_pass' => PASSWORD, // FROM CONSTANTS
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($_POST['email_id'], $_POST['first_name']); // change it to yours
        $this->email->to(RECEIPIENT_EMAIL);// change it to yours
        $this->email->subject('Purchase details');
        $mail_body = $this->load->view('email_template/purchase_mail_body', $_POST,TRUE);
        $this->email->message($mail_body);
        if($this->email->send()){
            redirect(base_url().'home', 'refresh');
        }
        else{
            show_error($this->email->print_debugger());
        }
    }
    public function contactMail(){
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => USERNAME, // FROM CONSTANTS
        'smtp_pass' => PASSWORD, // FROM CONSTANTS
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($_POST['email_id'], $_POST['first_name']); // change it to yours
        $this->email->to(RECEIPIENT_EMAIL);// change it to yours
        $this->email->subject('Contact details');
        $mail_body = $this->load->view('email_template/contact_mail_body', $_POST,TRUE);
        $this->email->message($mail_body);
        if($this->email->send()){
            redirect(base_url().'home', 'refresh');
        }
        else{
            show_error($this->email->print_debugger());
        }
    }
    
}


