<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('media_model');
        $this->load->model('schedule_class_model');
        if(!isset($_SESSION['user_name'])){
                $this->session->set_flashdata('authentication_failed', "You need to login to access this page");
                redirect(base_url().'login','refresh');
                exit;
        }
    }

	public function admin_dashboard(){
        if(isset($_SESSION['user_name'])){
            if($_SESSION['user_name'] == ""){
                $this->session->set_flashdata('authentication_failed', "You need to login to access this page");
                redirect(base_url().'login','refresh');
            }
        }
        $data['Title']="Home || Association Official Website";
        $data['Heading']="Dashboard";
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/admin_dashboard', $data);
        $this->load->view('admin_template/footer');
    }
    public function add_video(){
        $data['Title']="Add Video || Association Official Website";
        $data['Heading']="Add Video";
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/add_video', $data);
        $this->load->view('admin_template/footer');
    }
    public function add_video_action(){
        if($_POST){
            $url = $_POST['youtube_url'];
            parse_str( parse_url( $url, PHP_URL_QUERY ), $filtered_url );
            if(!isset($filtered_url['v'])){
               $this->session->set_flashdata('invalidEntry', "Please insert valid Url");
               redirect(base_url().'admin/add-video/','refresh');    
               exit;    
            }
            $youtube_url = $filtered_url['v']; 
            
            $data = array(
            'video_url' => $youtube_url,
            );
            $status = $this->media_model->insert_video($data);
            $insert_id = $this->db->insert_id();
            if ($insert_id){
                $this->session->set_flashdata('success', "Video Inserted Succesfully");
                redirect(base_url().'admin/add-video/','refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-video/','refresh');
            }
        }
    }
    public function list_video(){
        $data['Title']="List Video || Association Official Website";
        $data['Heading']="List Video";
		
		$this->load->library('pagination');
		$config['full_tag_open'] = "<ul class='pagination'>";
$config['full_tag_close'] ="</ul>";
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['next_tag_open'] = "<li>";
$config['next_tagl_close'] = "</li>";
$config['prev_tag_open'] = "<li>";
$config['prev_tagl_close'] = "</li>";
$config['first_tag_open'] = "<li>";
$config['first_tagl_close'] = "</li>";
$config['last_tag_open'] = "<li>";
$config['last_tagl_close'] = "</li>";
		 $config["base_url"] = base_url() . "admin/list-video/";
        $config["total_rows"] = $this->media_model->record_count_video();
        $config["per_page"] = 1;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		 $data["video_lists"] = $this->media_model->list_video($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
        //$data['video_lists'] = $this->media_model->list_video();
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/list_video', $data);
        $this->load->view('admin_template/footer');
    }
    public function update_video(){
        //echo "HELLLO"; exit;
        $data['Title']="Edit Video || Association Official Website";
        $data['Heading']="Edit Video";
        $id = $this->uri->segment(3);
        $data['video_data'] = $this->media_model->get_video_by_id($id);
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/edit_video', $data);
        $this->load->view('admin_template/footer');
    }
    public function update_video_action(){
        if($_POST){
            $id = $_POST['hid_id'];
            $url = $_POST['youtube_url'];
            parse_str( parse_url( $url, PHP_URL_QUERY ), $filtered_url );
            if(!isset($filtered_url['v'])){
               $this->session->set_flashdata('invalidEntry', "Please insert valid Url");
               redirect(base_url().'admin/add-video/','refresh');    
               exit;    
            }
            $youtube_url = $filtered_url['v']; 
            
            $data = array(
            'video_url' => $youtube_url,
            );
            $this->media_model->update_video($data,$id);
            $afftectedRows = $this->db->affected_rows();
            if ($afftectedRows == 1){
                $this->session->set_flashdata('success', "Video Updated Succesfully");
                redirect(base_url().'admin/list-video/','refresh');
            }
            else if ($afftectedRows == 0){
                $id = $_POST['hid_id'];
                $this->session->set_flashdata('already_updated', "Video Already Updated");
                redirect(base_url().'admin/update-video/'.$id,'refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-video/','refresh');
            }
        }
    }
    public function delete_video_action(){
        $id = $this->uri->segment(3);
        $this->media_model->delete_video($id);
        $this->session->set_flashdata('success', "Video Deleted");
        redirect(base_url().'admin/list-video/','refresh');
    }
	
	 public function add_image(){
        $data['Title']="Add Image || Association Official Website";
        $data['Heading']="Add Image";
		$data['category_li'] = $this->media_model->get_list_array_with_null("image_gallery_category",'id','name','','name');
		//print_r($data);exit;
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/add_image', $data);
        $this->load->view('admin_template/footer');
    }
	public function add_image_action()
		{       
			$this->load->library('upload');

			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']); 
			for($i=0; $i<$cpt; $i++)
			{        
  
				$_FILES['userfile']['name']= $files['userfile']['name'][$i];
				$_FILES['userfile']['type']= $files['userfile']['type'][$i];
				$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
				$_FILES['userfile']['error']= $files['userfile']['error'][$i];
				$_FILES['userfile']['size']= $files['userfile']['size'][$i];    

				$this->upload->initialize($this->set_upload_options());
				if(!$this->upload->do_upload())
				{
					
					$error=array('error'=>$this->upload->display_errors());
				
				}
				else
				{
					

					 $data=$this->upload->data();
					 $datas['image_url'] = $data['file_name'];
					 $datas['image_category_id'] = $_POST['image_category_id'];
				
					 $data=$this->media_model->insert_image($datas); 
					 $insert_id = $this->db->insert_id();
				}
			}
			if($insert_id){
			$this->session->set_flashdata('success', "Image added");
			redirect('admin/add-image','refresh');
			}
			else{
			$this->session->set_flashdata('failure', "Something went wrong, please try again");
			redirect('admin/add-image','refresh');
			}
		}

		private function set_upload_options()
		{   
			//upload an image options
			$config = array();
			$config['upload_path'] = './admin_assets/images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']      = '0';
			$config['overwrite']     = FALSE;

			return $config;
		}
		public function list_image(){
        $data['Title']="List Image || Association Official Website";
        $data['Heading']="List Image";
		$id = $this->uri->segment(3);
		
		$this->load->library('pagination');
		$config['full_tag_open'] = "<ul class='pagination'>";
$config['full_tag_close'] ="</ul>";
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['next_tag_open'] = "<li>";
$config['next_tagl_close'] = "</li>";
$config['prev_tag_open'] = "<li>";
$config['prev_tagl_close'] = "</li>";
$config['first_tag_open'] = "<li>";
$config['first_tagl_close'] = "</li>";
$config['last_tag_open'] = "<li>";
$config['last_tagl_close'] = "</li>";
		 $config["base_url"] = base_url() . "admin/list-image/".$id.'/';
        $config["total_rows"] = $this->media_model->record_count($id);
        $config["per_page"] = 2;
        $config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		//echo "<pre>";print_r($this->uri->segment(2));echo "</pre>";exit;
        $data["image_lists"] = $this->media_model->list_image($config["per_page"], $page,$id);
        $data["links"] = $this->pagination->create_links();

		
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/list_image', $data);
        $this->load->view('admin_template/footer');
    }
		 public function list_image_category(){
        $data['Title']="List Image Category || Association Official Website";
        $data['Heading']="List Image Category";
		
		$this->load->library('pagination');
		$config['full_tag_open'] = "<ul class='pagination'>";
$config['full_tag_close'] ="</ul>";
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
$config['next_tag_open'] = "<li>";
$config['next_tagl_close'] = "</li>";
$config['prev_tag_open'] = "<li>";
$config['prev_tagl_close'] = "</li>";
$config['first_tag_open'] = "<li>";
$config['first_tagl_close'] = "</li>";
$config['last_tag_open'] = "<li>";
$config['last_tagl_close'] = "</li>";
		 $config["base_url"] = base_url() . "admin/list-image-category/";
        $config["total_rows"] = $this->media_model->record_count_category();;
        $config["per_page"] = 2;
        $config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		 $data["image_lists"] = $this->media_model->list_image_category($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
        //$data['image_lists'] = $this->media_model->list_image_category();
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/list_image_category', $data);
        $this->load->view('admin_template/footer');
    }
	
	public function delete_image_action(){
        $id = $this->uri->segment(3);
        $this->media_model->delete_image($id);
        $this->session->set_flashdata('success', "image Deleted");
        redirect(base_url().'admin/list-image-category/','refresh');
    }
	
	public function update_image(){
        //echo "HELLLO"; exit;
        $data['Title']="Edit Image || Association Official Website";
        $data['Heading']="Edit Image";
        $id = $this->uri->segment(3);
        $data['image_data'] = $this->media_model->get_image_by_id($id);
		//$data['image_category_id'] = $this->media_model->get_image_by_id($id);
		$data['category_li'] = $this->media_model->get_list_array_with_null("image_gallery_category",'id','name','','name');
		
		//
		
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/edit_image', $data);
        $this->load->view('admin_template/footer');
    }
	 public function update_image_action(){
        if($_POST){
            $id = $_POST['hid_id'];
            $name = $_POST['hid_name'];
            $url = $_FILES["userfile"] ["name"];
			
            $data['image_url']= $url;
            $data['image_category_id']= $_POST['image_category_id'];
			if(@$_POST['image_category_id'] && $url==''){
				  $data['image_url'] = $name;
				$data['image_category_id'] = $_POST['image_category_id'];
				 $this->media_model->update_image($data,$id);
			}
          if($url){
            $this->media_model->update_image($data,$id);
			$ROOT = './admin_assets/images';
			unlink($ROOT.'/'.$name);
            $afftectedRows = $this->db->affected_rows();
            if ($afftectedRows == 1){
				
				move_uploaded_file($_FILES["userfile"]["tmp_name"],$_SERVER['DOCUMENT_ROOT']."/admin_assets/images/".$_FILES["userfile"] ["name"]) ;
                $this->session->set_flashdata('success', "Image Updated Succesfully");
                redirect(base_url().'admin/list-image-category/','refresh');
            }
            else if ($afftectedRows == 0){
                $id = $_POST['hid_id'];
                $this->session->set_flashdata('already_updated', "Image Already Updated");
                redirect(base_url().'admin/update-image/'.$id,'refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-image/','refresh');
            }
		  }
		  else
		  {
			  redirect(base_url().'admin/list-image-category/','refresh');
		  }
        }
    }
    public function add_category(){
        $data['Title']="Add Category || Association Official Website";
        $data['Heading']="Add Category";
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/add_category', $data);
        $this->load->view('admin_template/footer');
    }
	public function add_category_action(){
        if($_POST){
            $name = $_POST['name'];

            $data = array(
            'name' => $name,
            );
            $status = $this->media_model->insert_category($data);
            $insert_id = $this->db->insert_id();
            if ($insert_id){
                $this->session->set_flashdata('success', "Category Inserted Succesfully");
                redirect(base_url().'admin/add-category/','refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-category/','refresh');
            }
        }
    }
    /* New method */
    public function add_schedule_class(){
        $data['Title']="Add Class || Association Official Website";
        $data['Heading']="Add Class";
        $data["class_days"] = $this->schedule_class_model->list_all();
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/add_schedule_class', $data);
        $this->load->view('admin_template/footer');
    }
	public function add_class_action(){
        if($_POST){
            $class_day = $_POST['class_day'];
            $class_name = $_POST['class_name'];
            $class_from = $_POST['class_from'];
            $class_to = $_POST['class_to'];

            $data = array(
            'schedule_class_id' => $class_day,
            'class_name' => $class_name,
            'from_time' => $class_from,
            'to_time' => $class_to,
            );
            $status = $this->schedule_class_model->insert_data($data);
            $insert_id = $this->db->insert_id();
            if ($insert_id){
                $this->session->set_flashdata('success', "Class Inserted Succesfully");
                redirect(base_url().'admin/add-class/','refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-class/','refresh');
            }
        }
    }
    public function list_classes(){
        $data['Title']="List Classes || Association Official Website";
        $data['Heading']="List Classes";
        $id = $this->uri->segment(3);
        $data["class_days"] = $this->schedule_class_model->list_all();
        $data['list_classes'] = $this->schedule_class_model->list_classes_by_id($id);
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/list_class', $data);
        $this->load->view('admin_template/footer');
    }
    public function update_schedule_class(){
        $data['Title']="Update Class || Association Official Website";
        $data['Heading']="Update Class";
        $id = $this->uri->segment(3);
        $data["selected_class_day"] = $this->schedule_class_model->list_class_times_by_id($id);
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/update_schedule_class', $data);
        $this->load->view('admin_template/footer');
    }
    public function update_schedule_action(){
        if($_POST){
            $id = $_POST['hid_id'];
			$data['class_name'] = $_POST['class_name'];
			$data['from_time'] = $_POST['class_from'];
			$data['to_time'] = $_POST['class_to'];
            /*
            $class_name = $_POST['class_name'];
            $class_from = $_POST['class_from'];
            $class_to = $_POST['class_to'];
            */
            $this->schedule_class_model->update_data($id,$data);
		
            $afftectedRows = $this->db->affected_rows();
            if ($afftectedRows == 1){
				
				
                $this->session->set_flashdata('success', "Classes Updated Succesfully");
                redirect(base_url().'admin/list-class/1','refresh');
            }
            else if ($afftectedRows == 0){
                $id = $_POST['hid_id'];
                $this->session->set_flashdata('already_updated', "Classes Already Updated");
                redirect(base_url().'admin/update-class/'.$id,'refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-class/','refresh');
            }
		  }
		
        
	}
    public function delete_class(){
        $id = $this->uri->segment(3);
        $this->schedule_class_model->delete_data($id);
        $this->session->set_flashdata('success', "Class  Deleted");
        redirect(base_url().'admin/list-class/1','refresh');
    }
    /* New method */
	 public function list_category(){
        $data['Title']="List Category || Association Official Website";
        $data['Heading']="List Category";
        $data['category'] = $this->media_model->list_category();
		//print_r($data['category_lists']);exit;
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/list_category', $data);
        $this->load->view('admin_template/footer');
    }
	public function delete_category_action(){
        $id = $this->uri->segment(3);
        $this->media_model->delete_category($id);
        $this->session->set_flashdata('success', "Category  Deleted");
        redirect(base_url().'admin/list-category/','refresh');
    }
	public function update_category(){
        //echo "HELLLO"; exit;
        $data['Title']="Edit Category || Association Official Website";
        $data['Heading']="Edit Category";
        $id = $this->uri->segment(3);
        $data['category_data'] = $this->media_model->get_category_by_id($id);
		
        $this->load->view('admin_template/header',$data);
        $this->load->view('admin_template/nav',$data);
        $this->load->view('admin_pages/edit_category', $data);
        $this->load->view('admin_template/footer');
    }
	public function update_category_action(){
        if($_POST){
            $id = $_POST['hid_id'];
			$data['name'] = $_POST['name'];
         
            $this->media_model->update_category($data,$id);
		
            $afftectedRows = $this->db->affected_rows();
            if ($afftectedRows == 1){
				
				
                $this->session->set_flashdata('success', "Category Updated Succesfully");
                redirect(base_url().'admin/list-category/','refresh');
            }
            else if ($afftectedRows == 0){
                $id = $_POST['hid_id'];
                $this->session->set_flashdata('already_updated', "Category Already Updated");
                redirect(base_url().'admin/update-category/'.$id,'refresh');
            }else{
                $this->session->set_flashdata('failure', "Something went wrong, please try again");
                redirect(base_url().'admin/add-category/','refresh');
            }
		  }
		
        
	}
    
    public function logout(){
        $id = $this->uri->segment(3);
        $this->media_model->delete_video($id);
        $this->session->set_flashdata('success', "Video Deleted");
        redirect(base_url().'admin/list-video/','refresh');
    }
    
    
    
}


