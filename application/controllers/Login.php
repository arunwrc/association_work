<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
    }

	public function login_form(){
        if(isset($_SESSION['user_name'])){
                redirect(base_url().'admin','refresh');
                exit;
        }
        $data['Title']="Login || Association Official Website";
        $this->load->view('pages/login', $data);
    }
    public function login_action(){
        if($_POST){  
        $username = $_POST['user_name'];    
        $password = $_POST['password'];    
        $user=$this->user_model->login($username,md5($password));
            if(!$user){  // checking credentails, if wrong redirect to login with error meesage
                $this->session->set_flashdata('loginFailed', "Invalid Credentails, Please try again");
                redirect(base_url().'login/','refresh');
            }
            $this->session->set_userdata('id',$user['id']);
            $this->session->set_userdata('user_name',$user['username']);
            redirect(base_url().'admin/','refresh');
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url().'login/');
    }
}


