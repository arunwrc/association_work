<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {


	public function buy_service(){
        $data['Title']="Service || Association Official Website";
        $service_id = $this->uri->segment(2);
        $this->load->view('template/header',$data);
        $this->load->view('template/nav',$data);
        $this->load->view('pages/service_detail', $data);
        $this->load->view('template/footer');
    }
    
}


