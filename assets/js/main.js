/* Menu styles*/
$(".nav li").click(function(){
    $(this).find('a').addClass("newcs");
    var link = $(this).find('a').attr("href");
    window.location.href = link;
});
$(".logo_link").click(function(){
    window.location.href = "/home";
});
$(".nav_to_classes").click(function(){
    window.location.href = "/classes";
});

$(document).ready(function() {
    $('#dob')
    .datepicker({
        autoclose: true,
        format: 'mm/dd/yyyy'
    })
    $('#appointment_date')
    .datepicker({
        autoclose: true,
        format: 'mm/dd/yyyy'
    })
});

//$(".dropdown  li > a").click(function(){
//    $(this).next().toggle(200);
//    return false;
//});
$(".dropdown a").click(function(){
    //return false;
    (".menu_holder").attr("aria-expanded","true");
});

$(".schedule_item").click(function(){
    $(".schedule_item").removeClass("Selected"); //removes class Clicked
    $(".schedule_column_description").hide(); // hides all other blocks on click
    $(".schedule_column_description").removeClass("Clicked"); //removes class Clicked
    $(this).addClass("Selected");
    var shedule_day = $(this).attr("id");
    show_shedule_data(shedule_day); 
});


function show_shedule_data(shedule_day){
    var filtered_day = shedule_day.substring(shedule_day.indexOf('_') +1); // removes undescrore 
    $("#"+filtered_day).addClass("Clicked"); //add class Clicked
}

$(".btn-view-all-schedule").click(function(){
    $(".schedule_column_description").show();
    $(".schedule_item").removeClass("Selected");
});
$('#enrollment_form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
$('#appointment_form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
$('#contact_form').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

$().ready(function () {
  // validate signup form on keyup and submit
  $("#enrollment_form").validate({
    rules: {
      first_name: "required",
      last_name: "required",
      date: "required",
      address: "required",
      district: "required",
      //state: "required",
      email_id: {
        required: true,
        email: true
      },
      mobile_number: {
        required  : true,
        number: true,  
        minlength : 10,
        maxlength : 11
    },    
    },
    messages: {
      first_name: "Please enter your first name",
      last_name: "Please enter your last name",
      email_id: "Please enter a valid email address",
      date: "Please enter a date",
      mobile_number: "Please enter mobile number",
      address: "Please enter address",
      district: "Please enter district",
      state: "Please enter state",
    },
    });
    $("#buy_service_form").validate({
    rules: {
      first_name: "required",
      last_name: "required",
      address: "required",
      email_id: {
        required: true,
        email: true
      },
      mobile_number: {
        required  : true,
        number: true,  
        minlength : 10,
        maxlength : 11
    },    
    },
    messages: {
      first_name: "Please enter your first name",
      last_name: "Please enter your last name",
      email_id: "Please enter a valid email address",
      mobile_number: "Please enter mobile number",
      address: "Please enter address",
    },
    });
    $("#appointment_form").validate({
    rules: {
      first_name: "required",
      last_name: "required",
      date: "required",
      address: "required",
      district: "required",
      appointment_time: "required",
      appointment_date: "required",
      appointment_remark: "required",
      //state: "required",
      email_id: {
        required: true,
        email: true
      },
      mobile_number: {
        required  : true,
        number: true,  
        minlength : 10,
        maxlength : 11
    },    
    },
    messages: {
      first_name: "Please enter your first name",
      last_name: "Please enter your last name",
      email_id: "Please enter a valid email address",
      date: "Please enter a date",
      mobile_number: "Please enter mobile number",
      address: "Please enter address",
      district: "Please enter district",
      state: "Please enter state",
      appointment_time: "Please enter time",
      appointment_date: "Please enter date",
      appointment_remark: "Please enter remark",
    },
    });
  $("#contact_form").validate({
    rules: {
      first_name: "required",
      last_name: "required",
      date: "required",
      address: "required",
      contact_district: "required",
      //contact_state: "required",
      email_id: {
        required: true,
        email: true
      },
      mobile_number: {
        required  : true,
        number: true,  
        minlength : 10,
        maxlength : 11
    },    
    },
    messages: {
      first_name: "Please enter your first name",
      last_name: "Please enter your last name",
      email_id: "Please enter a valid email address",
      date: "Please enter a date",
      mobile_number: "Please enter mobile number",
      address: "Please enter address",
      district: "Please enter district",
      state: "Please enter state",
    },
  });

});
$(".reset_service_form").click(function(){
    $('#buy_service_form')[0].reset(); 
    return false;
});
$(".reset_enrollment_form").click(function(){
    $('#enrollment_form')[0].reset(); 
    return false;
});
$(".reset_contact_form").click(function(){
    $('#contact_form')[0].reset(); 
    return false;
});
$('input[name=member_type]').change(function(){
    var value = $( 'input[name=member_type]:checked' ).val();
    $("#selected_membership").text(value);
});
$('#appointment_time').timepicker({
    timeFormat: 'h:mm p',
    interval: 30,
    defaultTime: '11',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

function openModal() {
  document.getElementById('myModal').style.display = "block";
}
